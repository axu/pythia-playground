"""
inspect the dimuon mass spectrum
"""
import sys
from array import array

import ROOT as rt
import lhcbStyle
from lhcbStyle import my_colors

# load pythia lib
lib = "/Users/austinxu/workdir/pythia8244/lib"
sys.path.insert(0, lib)

labels = [
        "Charged","Hadron","Lepton","Meson","Baryon",
        "Pion","Kaon","Proton","Electron","Muon"
]
pids = {
        "Pion": 211,
        "Kaon": 321,
        "Proton": 2212,
        "Electron": 11,
        "Muon": 13,
}


def draw_compare(output,hists,isLog=False):
    c1 = rt.TCanvas("c1","")
    maxs = [h.GetMaximum() for h in hists]
    ceiling = 1.1*max(maxs)
    if isLog:
        rt.gPad.SetLogx()
    lg = rt.TLegend(0.6,0.6,0.9,0.9)
    lg.SetNColumns(1)
    lg.SetBorderSize(0)
    lg.SetTextFont(132)
    lg.SetTextSize(0.05)
    for ii in range(len(hists)):
        hists[ii].Draw("hist F same")
        hists[ii].SetMaximum(ceiling)
        hists[ii].SetFillColorAlpha(my_colors(ii),0.6)
        hists[ii].SetLineWidth(0)
        hists[ii].SetXTitle("nCharged")
        hists[ii].SetYTitle("Events")
        lg.AddEntry(hists[ii],'{}'.format(hists[ii].GetName().split('-')[-1]),'f')
    lg.Draw()
    c1.SaveAs(output)


def gen(tag,configs):
    # get pythia
    import pythia8
    pythia = pythia8.Pythia()

    # configure pythia
    pythia.readString("Beams:eCM = 14000.")
    for config in configs:
        pythia.readString(config)
    #pythia.readString("Next:numberShowEvent = 5")
    pythia.init()

    # book histogram
    f1 = rt.TFile("results/{0}.root".format(tag),"recreate")
    t1 = rt.TTree("t1","")
    m_dimuon = array('d',[0.])
    t1.Branch("m_dimuon",m_dimuon,"m_dimuon/D")
    mup_mid = array('i',[0])
    t1.Branch("mup_mid",mup_mid,"mup_mid/I")
    mun_mid = array('i',[0])
    t1.Branch("mun_mid",mun_mid,"mun_mid/I")

    # event loop
    nentries = int(1e1)
    for iEvent in range(nentries):
        if not pythia.next():
            continue
        # particle loop
        mup = []
        mun = []
        temp_mup_mid = []
        temp_mun_mid = []
        for prt in pythia.event:
            if prt.isFinal() and prt.isCharged():
                if prt.id()==pids["Muon"]:
                    mup.append( rt.TLorentzVector(prt.px(),prt.py(),prt.pz(),prt.e()) )
                    temp_mup_mid.append( prt.mother1() )
                if prt.id()==-pids["Muon"]:
                    mun.append( rt.TLorentzVector(prt.px(),prt.py(),prt.pz(),prt.e()) )
                    temp_mun_mid.append( prt.mother1() )
        if len(mup) and len(mun):
            print("Dimuon generated!")
            pythia.info.list()
            #pythia.process.list()
            #pythia.event.list()
            dimuon = mup[0] + mun[0]
            m_dimuon[0] = dimuon.M()
            mup_mid[0] = temp_mup_mid[0]
            mun_mid[0] = temp_mun_mid[0]
            t1.Fill()

    pythia.stat()
    t1.Write()
    f1.Close()


configs = {
        "SoftQCD-nonDiffractive":["SoftQCD:nonDiffractive = on"],
}
# generation
for tag in configs.keys():
    gen(tag,configs[tag])

"""
# plot
for tag in configs.keys():
    f1 = rt.TFile("results/charged_multiplicity_{0}.root".format(tag))
    t1 = f1.Get("t1")
    nbins = 500
    h1 = rt.TH1I("Charged","",nbins,0,nbins)
    t1.Project("Charged","Charged")
    draw_compare("results/charged_multiplicity_{0}_{1}.pdf".format(tag,"Charged"),[h1])
"""
