import numpy as np
import uproot as ur

import ROOT as rt

def get_edge_from_tree(infile,tree,branch,lq,rq):
    """Return a tuple of left and right edge.
    Args:
        infile (str): input file name
        tree (str):   TTree name
        branch (str): branch name
        lq (float):   left quantile
        rq (float):   right quantile
    """
    branch = ur.open(infile)[tree][branch].array()
    lq = np.quantile(branch,lq)
    rq = np.quantile(branch,rq)

    return (lq,rq)


def get_h1_from_tree(infiles,intrees,branches,titles,weights,nbins,lq=0,rq=1,AUTO=True,LOG=False):
    """Return a list of histograms.
    Args:
        infiles (list):  a list of input files
        intrees (list):  a list of TTree
        branches (list): a list of branches
        titles (list):   a list of title of hists
        weights (list):  a list of weights of hists
        nbins (int):     number of bins of hists
        lq (float):   left quantile
        rq (float):   right quantile
    """
    from math import log

    files = [rt.TFile(f) for f in infiles]
    trees = [f.Get(t) for f,t in zip(files,intrees)]
    if AUTO:
        edge = get_edge_from_tree(infiles[0],intrees[0],branches[0],lq,rq)
    else:
        edge = (lq,rq)
    hists = []
    for ii in range(len(branches)):
        if LOG:
            hists.append( rt.TH1D(f"h_{ii}",titles[ii],nbins,log(edge[0]),log(edge[1])) )
            hists[ii].Sumw2()
            trees[ii].Project(hists[ii].GetName(),f"log({branches[ii]})",weights[ii])
        else:
            hists.append( rt.TH1D(f"h_{ii}",titles[ii],nbins,edge[0],edge[1]) )
            hists[ii].Sumw2()
            trees[ii].Project(hists[ii].GetName(),branches[ii],weights[ii])
        #hists[ii].Scale(1.0/hists[ii].Integral())
        hists[ii].SetDirectory(0)

    return hists


def get_h2_from_tree(infile,intree,xbranch,ybranch,weights,nxbins,nybins,xlq=0,xrq=1,ylq=0,yrq=1,AUTO=True):
    """Return a 2D histogram.
    """
    from math import log

    files = rt.TFile(infile)
    tree = files.Get(intree)
    if AUTO:
        xedge = get_edge_from_tree(infile,intree,xbranch,xlq,xrq)
        yedge = get_edge_from_tree(infile,intree,ybranch,ylq,yrq)
    else:
        xedge = (xlq,xrq)
        yedge = (ylq,yrq)
    hist = rt.TH2D("h2","",nxbins,xedge[0],xedge[1],nybins,yedge[0],yedge[1])
    hist.Sumw2()
    tree.Project(hist.GetName(),f"{ybranch}:{xbranch}",weights)
    hist.SetDirectory(0)

    return hist


def draw_1d(output,hists,xtitle,ytitle="Candidates",FILL=False):
    """Draw 1D hist(s).
    """
    rt.gROOT.SetBatch(True)
    import lhcbStyle

    maxs = [h.GetMaximum() for h in hists]
    mins = [h.GetMinimum() for h in hists]
    ceiling = 1.15*max(maxs)
    floor = 0 if min(mins)>0 else -1.15*min(mins)
    c1 = rt.TCanvas('c1','c1')
    if 1.0*hists[0].GetMaximumBin()>0.5*hists[0].GetNbinsX():
        lg = rt.TLegend(0.20,0.70,0.40,0.90)
    else:
        lg = rt.TLegend(0.70,0.70,0.90,0.90)
    lg.SetNColumns(1)
    lg.SetBorderSize(0)
    lg.SetTextSize(0.06)
    lg.SetTextFont(132)
    colors = [rt.kBlue, rt.kRed, rt.kGreen+2, rt.kOrange, rt.kMagenta+1]
    markers = [20,21,22,23,29]
    for ii in range(len(hists)):
        hists[ii].SetXTitle(xtitle)
        hists[ii].SetYTitle(ytitle)
        hists[ii].SetMaximum(ceiling)
        hists[ii].SetMinimum(floor)
        if not FILL:
            hists[ii].Draw('same')
            hists[ii].SetLineColor(colors[ii%len(colors)])
            hists[ii].SetMarkerStyle(markers[ii%len(markers)])
            hists[ii].SetMarkerColor(colors[ii%len(colors)])
            lg.AddEntry(hists[ii].GetName(),hists[ii].GetTitle(),'pl')
        else:
            hists[ii].Draw('same hist F')
            hists[ii].SetFillColorAlpha(colors[ii%len(colors)],0.5)
            hists[ii].SetLineWidth(0)
            lg.AddEntry(hists[ii].GetName(),hists[ii].GetTitle(),'f')
    lg.Draw()
    c1.SaveAs(output)


def draw_2d(output,hist,xtitle,ytitle):
    """Draw 2D hist.
    """
    rt.gROOT.SetBatch(True)
    import lhcbStyle

    c1 = rt.TCanvas("c1","c1")
    rt.gPad.SetRightMargin(0.15)
    #rt.gStyle.SetPalette(rt.kDarkBodyRadiator)
    #rt.gStyle.SetPalette(rt.kInvertedDarkBodyRadiator)
    #rt.gStyle.SetPalette(rt.kBlackBody)
    rt.gStyle.SetPalette(rt.kLightTemperature)
    #rt.TColor.InvertPalette()
    hist.Draw("COL Z")
    hist.SetXTitle(xtitle)
    hist.SetYTitle(ytitle)
    c1.SaveAs(output)


if __name__=='__main__':
    import os
    import lhcbStyle
    # test 1d
    path = 'results'
    files = [os.path.join(path,'SoftQCD-nonDiffractive.root')]
    trees = ['t1']
    titles = ['Wide region']
    weights = ['1']
    nbins = 100
    branches = ['m_dimuon']
    hists = get_h1_from_tree(files,trees,branches,titles,weights,nbins,rq=0.9)
    draw_1d("results/m_dimuon_total.pdf",hists,'M(#mu^{+}#mu^{#minus}) [GeV/#it{c}^{2}]',FILL=True)
    titles = ['J/#psi']
    hists = get_h1_from_tree(files,trees,branches,titles,weights,nbins,lq=3.0969-0.001,rq=3.0969+0.001,AUTO=False)
    draw_1d("results/m_dimuon_jspi.pdf",hists,'M(#mu^{+}#mu^{#minus}) [GeV/#it{c}^{2}]',FILL=True)
    titles = ['#phi(1020)']
    hists = get_h1_from_tree(files,trees,branches,titles,weights,nbins,lq=1.01946-0.02,rq=1.01946+0.02,AUTO=False)
    draw_1d("results/m_dimuon_phi.pdf",hists,'M(#mu^{+}#mu^{#minus}) [GeV/#it{c}^{2}]',FILL=True)
    titles = ['#omega']
    hists = get_h1_from_tree(files,trees,branches,titles,weights,nbins,lq=0.78265-0.04,rq=0.78265+0.04,AUTO=False)
    draw_1d("results/m_dimuon_omega.pdf",hists,'M(#mu^{+}#mu^{#minus}) [GeV/#it{c}^{2}]',FILL=True)
    titles = ['#eta']
    hists = get_h1_from_tree(files,trees,branches,titles,weights,nbins,lq=0.54785-1*1e-5,rq=0.54785+1*1e-5,AUTO=False)
    draw_1d("results/m_dimuon_eta.pdf",hists,'M(#mu^{+}#mu^{#minus}) [GeV/#it{c}^{2}]',FILL=True)
    titles = ['Continuous']
    hists = get_h1_from_tree(files,trees,branches,titles,weights,nbins,lq=0.2,rq=0.50,AUTO=False)
    draw_1d("results/m_dimuon_cont.pdf",hists,'M(#mu^{+}#mu^{#minus}) [GeV/#it{c}^{2}]',FILL=True)
    titles = ['Wide region']
    branches = ['m_dimuon']
    weights = ['mup_mid==mun_mid']
    hists = get_h1_from_tree(files,trees,branches,titles,weights,nbins,rq=0.95)
    draw_1d("results/m_dimuon_true.pdf",hists,'M(#mu^{+}#mu^{#minus}) [GeV/#it{c}^{2}]',FILL=True)
    titles = ['High mass']
    branches = ['m_dimuon']
    weights = ['1']
    hists = get_h1_from_tree(files,trees,branches,titles,weights,nbins,lq=3.2,rq=10.5,AUTO=False)
    draw_1d("results/m_dimuon_highmass.pdf",hists,'M(#mu^{+}#mu^{#minus}) [GeV/#it{c}^{2}]',FILL=True)
    titles = ['#psi(2S) #Upsilon(1S) #Upsilon(2S) #Upsilon(3S)']
    branches = ['m_dimuon']
    weights = ['mup_mid==mun_mid']
    hists = get_h1_from_tree(files,trees,branches,titles,weights,nbins,lq=3.2,rq=10.5,AUTO=False)
    draw_1d("results/m_dimuon_highmass_true.pdf",hists,'M(#mu^{+}#mu^{#minus}) [GeV/#it{c}^{2}]',FILL=True)
