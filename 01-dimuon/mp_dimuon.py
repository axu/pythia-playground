"""
inspect the dimuon mass spectrum
"""
import sys
from array import array

import ROOT as rt
import lhcbStyle
from lhcbStyle import my_colors

# load pythia lib
lib = "/home/xuao/workdir/dev/pythia8244/lib"
sys.path.insert(0, lib)

labels = [
        "Charged","Hadron","Lepton","Meson","Baryon",
        "Pion","Kaon","Proton","Electron","Muon"
]
pids = {
        "Pion": 211,
        "Kaon": 321,
        "Proton": 2212,
        "Electron": 11,
        "Muon": 13,
}



def gen(seed):
    # get pythia
    import pythia8
    pythia = pythia8.Pythia()

    # configure pythia
    pythia.readString("Beams:eCM = 14000.")
    pythia.readString("SoftQCD:nonDiffractive = on")
    pythia.readString("Random:setSeed = on")
    pythia.readString("Random:seed = {0}".format(int(seed)))
    pythia.readString("Print:quiet = on")
    pythia.init()

    # book histogram
    f1 = rt.TFile("results/SoftQCD-nonDiffractive_{0}.root".format(seed),"recreate")
    t1 = rt.TTree("t1","")
    m_dimuon = array('d',[0.])
    t1.Branch("m_dimuon",m_dimuon,"m_dimuon/D")
    mup_mid = array('i',[0])
    t1.Branch("mup_mid",mup_mid,"mup_mid/I")
    mun_mid = array('i',[0])
    t1.Branch("mun_mid",mun_mid,"mun_mid/I")

    # event loop
    nentries = int(1e6)
    for iEvent in range(nentries):
        if not pythia.next():
            continue
        # particle loop
        mup = []
        mun = []
        temp_mup_mid = []
        temp_mun_mid = []
        for prt in pythia.event:
            if prt.isFinal() and prt.isCharged():
                if prt.id()==pids["Muon"]:
                    mup.append( rt.TLorentzVector(prt.px(),prt.py(),prt.pz(),prt.e()) )
                    temp_mup_mid.append( prt.mother1() )
                if prt.id()==-pids["Muon"]:
                    mun.append( rt.TLorentzVector(prt.px(),prt.py(),prt.pz(),prt.e()) )
                    temp_mun_mid.append( prt.mother1() )
        if len(mup) and len(mun):
            #print("Dimuon generated!")
            #pythia.info.list()
            #pythia.process.list()
            #pythia.event.list()
            dimuon = mup[0] + mun[0]
            m_dimuon[0] = dimuon.M()
            mup_mid[0] = temp_mup_mid[0]
            mun_mid[0] = temp_mun_mid[0]
            t1.Fill()

    pythia.stat()
    t1.Write()
    f1.Close()



if __name__ == '__main__':
    from multiprocessing import Pool
    import time
    start_time = time.time()
    nworkers = 50
    with Pool( processes=nworkers ) as pool:
        pool.map(gen,range(2020,2020+nworkers))
    print("--- {:.0f} seconds spent ---".format(time.time()-start_time))
