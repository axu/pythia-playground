"""
inspect the dimuon mass spectrum
"""
import sys
from array import array

import ROOT as rt
import lhcbStyle
from lhcbStyle import my_colors

# load pythia lib
lib = "/home/xuao/workdir/dev/pythia8244/lib"
sys.path.insert(0, lib)

labels = [
        "Charged","Hadron","Lepton","Meson","Baryon",
        "Pion","Kaon","Proton","Electron","Muon"
]
pids = {
        "Pion": 211,
        "Kaon": 321,
        "Proton": 2212,
        "Electron": 11,
        "Muon": 13,
        "Lambdab": 5122,
        "B0": 511
}
# Bd: 51?
# Bu: 52?
# Bs: 53?
# B baryon: 5???



def gen(seed):
    # get pythia
    import pythia8
    pythia = pythia8.Pythia()

    # configure pythia
    CM = 1000
    pythia.readString("Beams:eCM = {}".format(CM))
    pythia.readString("SoftQCD:nonDiffractive = on")
    pythia.readString("Random:setSeed = on")
    pythia.readString("Random:seed = {0}".format(int(seed)))
    pythia.readString("Print:quiet = on")
    #pythia.readString("Next:numberShowEvent = 5")
    pythia.init()

    # book histogram
    f1 = rt.TFile("results/SoftQCD-nonDiffractive_{0}_{1}.root".format(CM,seed),"recreate")
    t1 = rt.TTree("t1","")
    b_px = array('d',[0.])
    t1.Branch("b_px",b_px,"b_px/D")
    b_py = array('d',[0.])
    t1.Branch("b_py",b_py,"b_py/D")
    b_pz = array('d',[0.])
    t1.Branch("b_pz",b_pz,"b_pz/D")
    b_pt = array('d',[0.])
    t1.Branch("b_pt",b_pt,"b_pt/D")
    b_y = array('d',[0.])
    t1.Branch("b_y",b_y,"b_y/D")
    b_theta = array('d',[0.])
    t1.Branch("b_theta",b_theta,"b_theta/D")
    b_phi = array('d',[0.])
    t1.Branch("b_phi",b_phi,"b_phi/D")

    bbar_px = array('d',[0.])
    t1.Branch("bbar_px",bbar_px,"bbar_px/D")
    bbar_py = array('d',[0.])
    t1.Branch("bbar_py",bbar_py,"bbar_py/D")
    bbar_pz = array('d',[0.])
    t1.Branch("bbar_pz",bbar_pz,"bbar_pz/D")
    bbar_pt = array('d',[0.])
    t1.Branch("bbar_pt",bbar_pt,"bbar_pt/D")
    bbar_y = array('d',[0.])
    t1.Branch("bbar_y",bbar_y,"bbar_y/D")
    bbar_theta = array('d',[0.])
    t1.Branch("bbar_theta",bbar_theta,"bbar_theta/D")
    bbar_phi = array('d',[0.])
    t1.Branch("bbar_phi",bbar_phi,"bbar_phi/D")

    b_id = array('i',[0])
    t1.Branch("b_id",b_id,"b_id/I")
    bbar_id = array('i',[0])
    t1.Branch("bbar_id",bbar_id,"bbar_id/I")

    # event loop
    nentries = int(1e4)
    nb = 0
    for iEvent in range(nentries):
        if not pythia.next():
            continue
        # particle loop
        b_index = 0
        bbar_index = 0
        for ii in range(pythia.event.size()):
            prt = pythia.event[ii]
            # note: b quark and b meson, bbar quark and bbar meson have
            #       opposite sign of id
            isB = (prt.id()>-600 and prt.id()<-500) or (prt.id()>5000 and prt.id()<6000)
            isBbar = (prt.id()>500 and prt.id()<600) or (prt.id()>-6000 and prt.id()<-5000)
            if isB:
                b_index = ii
            if isBbar:
                bbar_index = ii
        if b_index and bbar_index:
            nb += 1
            #print("b hadron generated!")
            #if nb==2:
            #    for ii in range(pythia.event.size()):
            #        prt = pythia.event[ii]
            #        print(prt.index(),prt.id(),prt.mother1(),prt.mother2(),prt.daughter1(),prt.daughter2())
            prt_b = pythia.event[b_index]
            prt_bbar = pythia.event[bbar_index]
            b_id[0] = prt_b.id()
            b_px[0] = prt_b.px()
            b_py[0] = prt_b.py()
            b_pz[0] = prt_b.pz()
            b_pt[0] = prt_b.pT()
            b_y[0] = prt_b.y()
            b_theta[0] = prt_b.theta()
            b_phi[0] = prt_b.phi()
            bbar_id[0] = prt_bbar.id()
            bbar_px[0] = prt_bbar.px()
            bbar_py[0] = prt_bbar.py()
            bbar_pz[0] = prt_bbar.pz()
            bbar_pt[0] = prt_bbar.pT()
            bbar_y[0] = prt_bbar.y()
            bbar_theta[0] = prt_bbar.theta()
            bbar_phi[0] = prt_bbar.phi()

            t1.Fill()

    pythia.stat()
    print(f"{nb} b events in {nentries} events.")
    t1.Write()
    f1.Close()



if __name__ == '__main__':
    from multiprocessing import Pool
    import time
    start_time = time.time()
    nworkers = 50
    with Pool( processes=nworkers ) as pool:
        pool.map(gen,range(2020,2020+nworkers))
    print("--- {:.0f} seconds spent ---".format(time.time()-start_time))
