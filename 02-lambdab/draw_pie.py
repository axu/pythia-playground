import matplotlib.pyplot as plt

# Pie chart, where the slices will be ordered and plotted counter-clockwise:
labels = '$B_{d}$', '$B_{u}$', '$B_{s}$', '$B_{c}$', 'b baryon'
sizes = [1343337, 1341596, 303695, 562, 121586+15948+15918+512]
explode = (0, 0, 0, 0, 0.1)

fig1, ax1 = plt.subplots()
ax1.pie(sizes, explode=explode, labels=labels, autopct='%1.1f%%',
        shadow=False, startangle=90)
ax1.axis('equal')  # Equal aspect ratio ensures that pie is drawn as a circle.

#plt.show()
plt.savefig('plots/fraction.pdf')
