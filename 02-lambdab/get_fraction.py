import ROOT as rt

def get_fraction(pid):
    f1 = rt.TFile("results/SoftQCD-nonDiffractive_14000.root")
    t1 = f1.Get("t1")

    pid = []
    nentries = t1.GetEntries()
    for ii in range(nentries):
        t1.GetEntry(ii)
        b_id = abs(t1.b_id)
        if b_id not in pid:
            pid.append( b_id )
        b_id = abs(t1.bbar_id)
        if b_id not in pid:
            pid.append( b_id )

    print(pid)

    ncan = []
    for key in pid:
        n = t1.GetEntries(f"abs(b_id)=={key}")+t1.GetEntries(f"abs(bbar_id)=={key}")
        ncan.append( n )
        print(key,n)
    print(sum(ncan))


if __name__=="__main__":
    get_fraction([])
