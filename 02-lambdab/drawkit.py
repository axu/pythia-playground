import numpy as np
import uproot as ur

import ROOT as rt

def get_edge_from_tree(infile,tree,branch,lq,rq):
    """Return a tuple of left and right edge.
    Args:
        infile (str): input file name
        tree (str):   TTree name
        branch (str): branch name
        lq (float):   left quantile
        rq (float):   right quantile
    """
    branch = ur.open(infile)[tree][branch].array()
    lq = np.quantile(branch,lq)
    rq = np.quantile(branch,rq)

    return (lq,rq)


def get_h1_from_tree(infiles,intrees,branches,titles,weights,nbins,lq=0,rq=1,AUTO=True,LOG=False):
    """Return a list of histograms.
    Args:
        infiles (list):  a list of input files
        intrees (list):  a list of TTree
        branches (list): a list of branches
        titles (list):   a list of title of hists
        weights (list):  a list of weights of hists
        nbins (int):     number of bins of hists
        lq (float):   left quantile
        rq (float):   right quantile
    """
    from math import log

    files = [rt.TFile(f) for f in infiles]
    trees = [f.Get(t) for f,t in zip(files,intrees)]
    if AUTO:
        edge = get_edge_from_tree(infiles[0],intrees[0],branches[0],lq,rq)
    else:
        edge = (lq,rq)
    hists = []
    for ii in range(len(branches)):
        if LOG:
            hists.append( rt.TH1D(f"h_{ii}",titles[ii],nbins,log(edge[0]),log(edge[1])) )
            hists[ii].Sumw2()
            trees[ii].Project(hists[ii].GetName(),f"log({branches[ii]})",weights[ii])
        else:
            hists.append( rt.TH1D(f"h_{ii}",titles[ii],nbins,edge[0],edge[1]) )
            hists[ii].Sumw2()
            trees[ii].Project(hists[ii].GetName(),branches[ii],weights[ii])
        hists[ii].Scale(1.0/hists[ii].Integral())
        hists[ii].SetDirectory(0)

    return hists


def get_h2_from_tree(infile,intree,xbranch,ybranch,weights,nxbins,nybins,xlq=0,xrq=1,ylq=0,yrq=1,AUTO=True):
    """Return a 2D histogram.
    """
    from math import log

    files = rt.TFile(infile)
    tree = files.Get(intree)
    if AUTO:
        xedge = get_edge_from_tree(infile,intree,xbranch,xlq,xrq)
        yedge = get_edge_from_tree(infile,intree,ybranch,ylq,yrq)
    else:
        xedge = (xlq,xrq)
        yedge = (ylq,yrq)
    hist = rt.TH2D("h2","",nxbins,xedge[0],xedge[1],nybins,yedge[0],yedge[1])
    hist.Sumw2()
    tree.Project(hist.GetName(),f"{ybranch}:{xbranch}",weights)
    hist.SetDirectory(0)

    return hist


def draw_1d(output,hists,xtitle,ytitle="Candidates",FILL=False,LOG=False):
    """Draw 1D hist(s).
    """
    rt.gROOT.SetBatch(True)
    import lhcbStyle

    maxs = [h.GetMaximum() for h in hists]
    mins = [h.GetMinimum() for h in hists]
    ceiling = 1.15*max(maxs)
    floor = 0 if min(mins)>0 else -1.15*min(mins)
    c1 = rt.TCanvas('c1','c1')
    if 1.0*hists[0].GetMaximumBin()>0.5*hists[0].GetNbinsX():
        lg = rt.TLegend(0.20,0.70,0.40,0.90)
    else:
        lg = rt.TLegend(0.70,0.70,0.90,0.90)
    lg.SetNColumns(1)
    lg.SetBorderSize(0)
    lg.SetTextSize(0.06)
    lg.SetTextFont(132)
    colors = [rt.kBlue, rt.kRed, rt.kGreen+2, rt.kOrange, rt.kMagenta+1]
    markers = [20,21,22,23,29]
    for ii in range(len(hists)):
        hists[ii].SetXTitle(xtitle)
        hists[ii].SetYTitle(ytitle)
        hists[ii].SetMaximum(ceiling)
        hists[ii].SetMinimum(floor)
        if not FILL:
            hists[ii].Draw('same')
            hists[ii].SetLineColor(colors[ii%len(colors)])
            hists[ii].SetMarkerStyle(markers[ii%len(markers)])
            hists[ii].SetMarkerColor(colors[ii%len(colors)])
            lg.AddEntry(hists[ii].GetName(),hists[ii].GetTitle(),'pl')
        else:
            hists[ii].Draw('same hist F')
            hists[ii].SetFillColorAlpha(colors[ii%len(colors)],0.5)
            hists[ii].SetLineWidth(0)
            lg.AddEntry(hists[ii].GetName(),hists[ii].GetTitle(),'f')
    lg.Draw()
    if LOG:
        rt.gPad.SetLogy()
    c1.SaveAs(output)


def draw_2d(output,hist,xtitle,ytitle):
    """Draw 2D hist.
    """
    rt.gROOT.SetBatch(True)
    import lhcbStyle

    c1 = rt.TCanvas("c1","c1")
    rt.gPad.SetRightMargin(0.15)
    #rt.gStyle.SetPalette(rt.kDarkBodyRadiator)
    #rt.gStyle.SetPalette(rt.kInvertedDarkBodyRadiator)
    #rt.gStyle.SetPalette(rt.kBlackBody)
    rt.gStyle.SetPalette(rt.kLightTemperature)
    #rt.TColor.InvertPalette()
    hist.Draw("COL Z")
    #hist.Draw("LEGO2")
    hist.SetXTitle(xtitle)
    hist.SetYTitle(ytitle)
    #rt.gPad.SetLogz()
    c1.SaveAs(output)


def draw_profile(output,hist,xtitle,ytitle,infile,intree,xbranch,ybranch,nxbins,nybins,xlq=0,xrq=1,ylq=0,yrq=1):
    """Draw 2D hist.
    """
    rt.gROOT.SetBatch(True)
    import lhcbStyle

    from array import array
    files = rt.TFile(infile)
    tree = files.Get(intree)
    # get means for y in x bins
    xbin = array('d',np.linspace(xlq,xrq,nxbins+1))
    ybin = array('d',np.linspace(ylq,yrq,nybins+1))
    hmean = rt.TH1D('hmean','',nxbins,xbin)
    for ii in range(nxbins):
        htemp = rt.TH1D('htemp','',nybins, ybin)
        tree.Project('htemp',ybranch,f'({xvar})>{xbin[ii]} && ({xvar})<{xbin[ii+1]}')
        hmean.SetBinContent( ii+1, htemp.GetMean(1) )
        hmean.SetBinError(   ii+1, htemp.GetMeanError(1) )


    c1 = rt.TCanvas("c1","c1")
    rt.gPad.SetRightMargin(0.15)
    #rt.gStyle.SetPalette(rt.kDarkBodyRadiator)
    #rt.gStyle.SetPalette(rt.kInvertedDarkBodyRadiator)
    #rt.gStyle.SetPalette(rt.kBlackBody)
    rt.gStyle.SetPalette(rt.kLightTemperature)
    #rt.TColor.InvertPalette()
    hist.Draw("COL Z")
    #hist.Draw("LEGO2")
    hist.SetXTitle(xtitle)
    hist.SetYTitle(ytitle)
    hmean.Draw('same')
    color = rt.kAzure+1
    hmean.SetLineColor(color)
    hmean.SetMarkerColor(color)
    #rt.gPad.SetLogz()
    c1.SaveAs(output)


if __name__=='__main__':
    import os
    import lhcbStyle

    # test 1d
    path = 'results'
    files = [os.path.join(path,'SoftQCD-nonDiffractive_14000.root'),
             os.path.join(path,'SoftQCD-nonDiffractive_14000.root'),]
    trees = 2*['t1']
    titles = ['#Lambda_{b}','B_{d}']
    weights = ['abs(b_id)==5122','abs(b_id)==511']
    nbins = 80
    variable = 'b_pt'
    branch = variable
    branches = 2*[branch]
    hists = get_h1_from_tree(files,trees,branches,titles,weights,nbins,rq=0.99)
    draw_1d(f"plots/{branch}.pdf",hists,'p_{T} [GeV]',FILL=True)

    nbins = 50
    variable = 'b_y'
    branch = variable
    branches = 2*[branch]
    hists = get_h1_from_tree(files,trees,branches,titles,weights,nbins)
    draw_1d(f"plots/{branch}.pdf",hists,'y',FILL=True)

    nbins = 80
    variable = 'sqrt(b_px*b_px+b_py*b_py+b_pz*b_pz)'
    branch = variable
    branches = 2*[branch]
    hists = get_h1_from_tree(files,trees,branches,titles,weights,nbins,lq=0,rq=150,AUTO=False)
    draw_1d("plots/b_p.pdf",hists,'P [GeV]',FILL=True)

    files = [os.path.join(path,'SoftQCD-nonDiffractive_14000.root')]
    trees = ['t1']
    titles = ['b and #bar{b}']
    weights = ['1']
    nbins = 80
    variable = 'acos((b_px*bbar_px+b_py*bbar_py+b_pz*bbar_pz) / (sqrt(b_px*b_px+b_py*b_py+b_pz*b_pz)*sqrt(bbar_px*bbar_px+bbar_py*bbar_py+bbar_pz*bbar_pz)))'
    branch = variable
    branches = [branch]
    hists = get_h1_from_tree(files,trees,branches,titles,weights,nbins,lq=0,rq=np.pi,AUTO=False)
    draw_1d("plots/b_delta-theta.pdf",hists,'#Delta #theta',FILL=True)

    path = 'results'
    infile = os.path.join(path,'SoftQCD-nonDiffractive_14000.root')
    intree = 't1'
    xvar = 'sqrt(b_px*b_px+b_py*b_py+b_pz*b_pz)'
    yvar = 'sqrt(bbar_px*bbar_px+bbar_py*bbar_py+bbar_pz*bbar_pz)'
    weight = '1'
    hist = get_h2_from_tree(infile,intree,xvar,yvar,weight,50,50,xlq=0,xrq=30,ylq=0,yrq=30,AUTO=False)
    draw_profile('plots/p_2d.pdf',hist,'P_{1} [GeV]','P_{2} [GeV]',infile,intree,xvar,yvar,50,50,xlq=0,xrq=30,ylq=0,yrq=30)

    path = 'results'
    infile = os.path.join(path,'SoftQCD-nonDiffractive_14000.root')
    intree = 't1'
    xvar = 'b_pt'
    yvar = 'bbar_pt'
    weight = '1'
    hist = get_h2_from_tree(infile,intree,xvar,yvar,weight,50,50,xlq=0,xrq=10,ylq=0,yrq=10,AUTO=False)
    draw_profile('plots/pt_2d.pdf',hist,'PT_{1} [GeV]','PT_{2} [GeV]',infile,intree,xvar,yvar,50,50,xlq=0,xrq=10,ylq=0,yrq=10)

    """
    path = 'results'
    infile = os.path.join(path,'SoftQCD-nonDiffractive_14000.root')
    intree = 't1'
    xvar = 'b_theta'
    yvar = 'bbar_theta'
    weight = '1'
    hist = get_h2_from_tree(infile,intree,xvar,yvar,weight,50,50)
    draw_2d('plots/theta_2d.pdf',hist,'#theta_{1}','#theta_{2}')
    """
