from array import array
import numpy as np
import uproot as ur

import ROOT as rt


rt.gROOT.SetBatch(True)
import lhcbStyle

def get_xsection(x):
    f1 = rt.TFile("results/SoftQCD-nonDiffractive_{}000.root".format(int(x)))
    t1 = f1.Get('t1')
    n1 = t1.GetEntries()
    #print(n1,n1/(50*1e4))
    print(n1/(50*1e4))


xx = [1,7,14,20,30]
yy = [0.00082, 0.008476, 0.015716, 0.021338, 0.029202]
yy = [y*100 for y in yy]
#for x in xx:
#    get_xsection(x)
xx = array('d',xx)
yy = array('d',yy)

c1 = rt.TCanvas('c1','c1')
gr = rt.TGraph(len(xx),xx,yy)
gr.GetYaxis().SetRangeUser(0,3.2)
gr.GetXaxis().SetTitle("#sqrt{s} [TeV]")
gr.GetYaxis().SetTitle("#sigma(pp #rightarrow b#bar{b} X) / #sigma_{ND} [%]")
gr.Draw("AC*")
gr.SetMarkerSize(3)
gr.SetMarkerColor(rt.kBlue)
c1.SaveAs("plots/xsection.pdf")
