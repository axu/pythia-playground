"""
inspect the charged multiplicity
"""
import sys
from array import array

import ROOT as rt
import lhcbStyle
from lhcbStyle import my_colors

# load pythia lib
lib = "/Users/austinxu/workdir/pythia8244/lib"
sys.path.insert(0, lib)

labels = [
        "Charged","Hadron","Lepton","Meson","Baryon",
        "Pion","Kaon","Proton","Electron","Muon"
]
pids = {
        "Pion": 211,
        "Kaon": 321,
        "Proton": 2212,
        "Electron": 11,
        "Muon": 13,
}


def draw_compare(output,hists,isLog=False):
    c1 = rt.TCanvas("c1","")
    maxs = [h.GetMaximum() for h in hists]
    ceiling = 1.1*max(maxs)
    if isLog:
        rt.gPad.SetLogx()
    lg = rt.TLegend(0.6,0.6,0.9,0.9)
    lg.SetNColumns(1)
    lg.SetBorderSize(0)
    lg.SetTextFont(132)
    lg.SetTextSize(0.05)
    for ii in range(len(hists)):
        hists[ii].Draw("hist F same")
        hists[ii].SetMaximum(ceiling)
        hists[ii].SetFillColorAlpha(my_colors(ii),0.6)
        hists[ii].SetLineWidth(0)
        hists[ii].SetXTitle("nCharged")
        hists[ii].SetYTitle("Events")
        lg.AddEntry(hists[ii],'{}'.format(hists[ii].GetName().split('-')[-1]),'f')
    lg.Draw()
    c1.SaveAs(output)

def draw_compare_log(output,hists):
    c1 = rt.TCanvas("c1","")
    maxs = [h.GetMaximum() for h in hists]
    ceiling = 1.1*max(maxs)
    #rt.gPad.SetLogx()
    rt.gPad.SetLogy()
    lg = rt.TLegend(0.6,0.6,0.9,0.9)
    lg.SetNColumns(1)
    lg.SetBorderSize(0)
    lg.SetTextFont(132)
    lg.SetTextSize(0.05)
    for ii in range(len(hists)):
        hists[ii].Draw("hist F same")
        hists[ii].SetMaximum(ceiling)
        hists[ii].SetFillColorAlpha(my_colors(ii),0.6)
        hists[ii].SetLineWidth(0)
        hists[ii].SetXTitle("nCharged")
        hists[ii].SetYTitle("Events")
        lg.AddEntry(hists[ii],'{}'.format(hists[ii].GetName().split('-')[-1]),'f')
    lg.Draw()
    c1.SaveAs(output)


def gen(tag,configs):
    # get pythia
    import pythia8
    pythia = pythia8.Pythia()

    # configure pythia
    pythia.readString("Beams:eCM = 14000.")
    for config in configs:
        pythia.readString(config)
    pythia.init()

    # book histogram
    f1 = rt.TFile("results/charged_multiplicity_{0}.root".format(tag),"recreate")
    t1 = rt.TTree("t1","")
    # new_branch = array('d',[0.])
    # t1.Branch('new_branch',new_branch,'new_branch/D')
    branches = {}
    for label in labels:
        branches[label] = array('i',[0])
        t1.Branch("label",branches[label],"{}/I".format(label))

    # event loop
    nentries = int(1e6)
    for iEvent in range(nentries):
        if not pythia.next():
            continue
        # particle loop
        counters = {}
        for label in labels:
            counters[label] = 0
        for prt in pythia.event:
            if prt.isFinal() and prt.isCharged():
                counters["Charged"] += 1
                if abs(prt.id())==pids["Pion"]:
                    counters["Pion"] += 1
                    counters["Hadron"] += 1
                    counters["Meson"] += 1
                if abs(prt.id())==pids["Kaon"]:
                    counters["Kaon"] += 1
                    counters["Hadron"] += 1
                    counters["Meson"] += 1
                if abs(prt.id())==pids["Proton"]:
                    counters["Proton"] += 1
                    counters["Hadron"] += 1
                    counters["Baryon"] += 1
                if abs(prt.id())==pids["Electron"]:
                    counters["Electron"] += 1
                    counters["Lepton"] += 1
                if abs(prt.id())==pids["Muon"]:
                    counters["Muon"] += 1
                    counters["Lepton"] += 1
        for label in labels:
            branches[label][0] = counters[label]
        t1.Fill()

    pythia.stat()
    t1.Write()
    f1.Close()


configs = {
        "SoftQCD-nonDiffractive":["SoftQCD:nonDiffractive = on"],
        "SoftQCD-doubleDiffractive": ["SoftQCD:doubleDiffractive = on"],
        "SoftQCD-singleDiffractive": ["SoftQCD:singleDiffractive = on"],
        "SoftQCD-centralDiffractive": ["SoftQCD:centralDiffractive = on","SigmaTotal:zeroAXB = off"],
        #"SoftQCD-elastic": ["SoftQCD:elastic = on"],
        #"HardQCD-all":["HardQCD:all = on","PhaseSpace:pTHatMin = 20."],
        #"HardQCD-bbbar":["HardQCD:hardbbbar = on","PhaseSpace:pTHatMin = 20."],
        #"HardQCD-ccbar":["HardQCD:hardccbar = on","PhaseSpace:pTHatMin = 20."]
}

"""
# generation
for tag in configs.keys():
    gen(tag,configs[tag])
"""


# draw muon
tag = "SoftQCD-nonDiffractive"
f1 = rt.TFile("results/charged_multiplicity_{0}.root".format(tag))
t1 = f1.Get("t1")
nbins = 5
h1 = rt.TH1I("Muon","Muon",nbins,2,nbins+2)
t1.Project("Muon","Muon","Muon>1")
draw_compare("results/charged_multiplicity_{0}_Muon.pdf".format(tag),[h1])

"""
# plot
for tag in configs.keys():
    f1 = rt.TFile("results/charged_multiplicity_{0}.root".format(tag))
    t1 = f1.Get("t1")
    nbins = 500
    h1 = rt.TH1I("Charged","",nbins,0,nbins)
    t1.Project("Charged","Charged")
    draw_compare("results/charged_multiplicity_{0}_{1}.pdf".format(tag,"Charged"),[h1])
"""

"""
# compare xsec
f = []
t = []
h = []
tags = ["SoftQCD-nonDiffractive","SoftQCD-doubleDiffractive","SoftQCD-centralDiffractive","SoftQCD-singleDiffractive"]
for ii in range(4):
    tag = tags[ii]
    f.append(rt.TFile("results/charged_multiplicity_{0}.root".format(tag)))
    t.append(f[ii].Get("t1"))
    nbins = 500
    h.append(rt.TH1I(tag,"",nbins,0,nbins))
    t[ii].Project(tag,"Charged")
draw_compare("results/charged_multiplicity_SoftQCD_Charged.pdf",h,True)
"""

"""
# compare tracks
tag = "SoftQCD-nonDiffractive"
f1 = rt.TFile("results/charged_multiplicity_{0}.root".format(tag))
t1 = f1.Get("t1")
nbins = 350
h = []
labels = ["Pion","Kaon","Proton","Electron"]
for label in labels:
    h.append(rt.TH1I(label,"",nbins,0,nbins))
    t1.Project(label,label)
draw_compare_log("results/charged_multiplicity_SoftQCD-nonDiffractive_pid.pdf",h)
"""
