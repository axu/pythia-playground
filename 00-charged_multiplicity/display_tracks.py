"""
inspect the charged multiplicity
"""
import sys
import math

import ROOT as rt
import lhcbStyle
from lhcbStyle import my_colors

# load pythia lib
lib = "/Users/austinxu/workdir/pythia8244/lib"
sys.path.insert(0, lib)

def get_end_yz(x,y,z,px,py,pz):
    t = py/pz
    frame_z = 100
    frame_y = 50
    if pz>0:
        temp_y = y+t*(frame_z-z)
        temp_z = z+1/t*(frame_y-y)
        if abs(temp_y)<50:
            return frame_z,temp_y
        else:
            return temp_z,frame_y


def get_line_yz(x,y,z,px,py,pz):
    t = 10000
    end_z = z + pz/math.sqrt(py**2+pz**2)*t
    end_y = y + py/math.sqrt(py**2+pz**2)*t
    return rt.TLine(z,y,end_z,end_y)

def get_line_xy(x,y,z,px,py,pz):
    t = 250
    end_x = x + px/math.sqrt(py**2+px**2)*t
    end_y = y + py/math.sqrt(py**2+px**2)*t
    return rt.TLine(x,y,end_x,end_y)

def draw_lines_yz(output,lines,pid):
    c1 = rt.TCanvas("c1","",800,600)
    c1.SetFrameLineWidth(0)
    xedge = 500
    yedge = 250
    hdummy = rt.TH1D("hdummy","",1,-1*xedge,xedge)
    hdummy.SetMaximum(yedge)
    hdummy.SetMinimum(-1*yedge)
    hdummy.Draw("A")
    colors = {'211': rt.kBlue,
              '321': rt.kRed,
              '2212': rt.kGreen,
              '11': rt.kOrange,
              '22': rt.kOrange}
    for ii in range(len(lines)):
        if abs(pid[ii]) not in [211,321,2212,11,22]:
            continue
        lines[ii].SetLineColorAlpha(colors[str(abs(pid[ii]))],0.6)
        lines[ii].SetLineWidth(2)
        lines[ii].Draw()
        #if abs(pid[ii])==22:
        #    lines[ii].SetLineStyle(rt.kDashed)
    c1.SaveAs(output)

def draw_lines_xy(output,lines,pid):
    c1 = rt.TCanvas("c1","",600,600)
    c1.SetFrameLineWidth(0)
    c1.SetMargin(0,0,0,0)
    xedge = 250
    yedge = 250
    hdummy = rt.TH1D("hdummy","",1,-1*xedge,xedge)
    hdummy.SetMaximum(yedge)
    hdummy.SetMinimum(-1*yedge)
    hdummy.Draw("A")
    circle = rt.TEllipse(0,0,xedge,xedge)
    circle.SetLineWidth(2)
    circle.SetLineStyle(rt.kDashed)
    circle.SetLineColorAlpha(rt.kBlack,0.5)
    circle.Draw()
    colors = {'211': rt.kBlue,
              '321': rt.kRed,
              '2212': rt.kGreen,
              '11': rt.kOrange,
              '22': rt.kOrange}
    for ii in range(len(lines)):
        if abs(pid[ii]) not in [211,321,2212,11,22]:
            continue
        lines[ii].SetLineColorAlpha(colors[str(abs(pid[ii]))],0.6)
        lines[ii].SetLineWidth(2)
        lines[ii].Draw()
        #if abs(pid[ii])==22:
        #    lines[ii].SetLineStyle(rt.kDashed)
    c1.SaveAs(output)

def gen(tag,configs):
    # get pythia
    import pythia8
    pythia = pythia8.Pythia()

    # configure pythia
    pythia.readString("Beams:eCM = 14000.")
    for config in configs:
        pythia.readString(config)
    pythia.init()

    # event loop
    nentries = int(1e1)
    for iEvent in range(nentries):
        if not pythia.next():
            continue
        # book lines
        lines_yz = []
        lines_xy = []
        pid = []
        # particle loop
        for prt in pythia.event:
            if prt.isFinal() and prt.isCharged():
                lines_yz.append( get_line_yz(prt.xProd(),prt.yProd(),prt.zProd(),prt.px(),prt.py(),prt.pz()) )
                lines_xy.append( get_line_xy(prt.xProd(),prt.yProd(),prt.zProd(),prt.px(),prt.py(),prt.pz()) )
                pid.append( prt.id() )
        draw_lines_yz("results/event_{0}_{1}_yz.pdf".format(tag,iEvent),lines_yz,pid)
        draw_lines_xy("results/event_{0}_{1}_xy.pdf".format(tag,iEvent),lines_xy,pid)

    pythia.stat()


def gen_bbbar(tag,configs):
    # get pythia
    import pythia8
    pythia = pythia8.Pythia()

    # configure pythia
    pythia.readString("Beams:eCM = 14000.")
    for config in configs:
        pythia.readString(config)
    pythia.init()

    # event loop
    nentries = int(1e3)
    for iEvent in range(nentries):
        if not pythia.next():
            continue
        # book lines
        lines_yz = []
        lines_xy = []
        pid = []
        # particle loop
        iBottom = 0
        for prt in pythia.event:
            if abs(prt.id())==5:
                iBottom += 1
            if prt.isFinal() and prt.isCharged():
                lines_yz.append( get_line_yz(prt.xProd(),prt.yProd(),prt.zProd(),prt.px(),prt.py(),prt.pz()) )
                lines_xy.append( get_line_xy(prt.xProd(),prt.yProd(),prt.zProd(),prt.px(),prt.py(),prt.pz()) )
                pid.append( prt.id() )
        if iBottom:
            draw_lines_yz("results/event_{0}_bbbar_{1}_yz.pdf".format(tag,iEvent),lines_yz,pid)
            draw_lines_xy("results/event_{0}_bbbar_{1}_xy.pdf".format(tag,iEvent),lines_xy,pid)

    pythia.stat()



configs = {
        "SoftQCD-nonDiffractive":["SoftQCD:nonDiffractive = on"],
        "SoftQCD-doubleDiffractive": ["SoftQCD:doubleDiffractive = on"],
        "SoftQCD-singleDiffractive": ["SoftQCD:singleDiffractive = on"],
        "SoftQCD-centralDiffractive": ["SoftQCD:centralDiffractive = on","SigmaTotal:zeroAXB = off"],
        "SoftQCD-elastic": ["SoftQCD:elastic = on"],
        #"HardQCD-all":["HardQCD:all = on","PhaseSpace:pTHatMin = 20."],
        #"HardQCD-bbbar":["HardQCD:hardbbbar = on","PhaseSpace:pTHatMin = 20."],
        #"HardQCD-ccbar":["HardQCD:hardccbar = on","PhaseSpace:pTHatMin = 20."]
}

#gen_bbbar("SoftQCD-nonDiffractive",["SoftQCD:nonDiffractive = on"])

# generation
for tag in configs.keys():
    gen(tag,configs[tag])
